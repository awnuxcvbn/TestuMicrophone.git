using UnityEngine;
using uMicrophone;

public class Recorder : MonoBehaviour
{
    [SerializeField]
    private SignalManager signalManager;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        signalManager.onAudioClipDone = null;
        signalManager.onAudioClipDone += OnAudioClip;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 300, 300), "Start"))
        {
            signalManager.StartRecordBinding();
        }
        if (GUI.Button(new Rect(0, 300, 300, 300), "Stop"))
        {
            signalManager.StopRecordBinding();
        }
        if (GUI.Button(new Rect(0, 600, 300, 300), "Play"))
        {
            audioSource.Play();
        }
    }

    void OnAudioClip(AudioClip audioClip)
    {
        audioSource.clip = audioClip;
    }
}